# Boilerplate 🧪 Flask + GCP Cloud Run + Docker + GitLab

## General Idea
![Boilerplate Overview](https://cityguessr.fun/FCRDG.png){width=50%}

* Python/Flask to create your website/app
* Source code in GitLab
* Build (docker image) and deploy from GitLab pipelines to GCP Cloud Run
  * Everytime you commit to main it will auto-deploy to production
* Your website/app runs in GCP Cloud Run, people can access it there!

This is a very minimal boilerplate that gives you hopefully enough to get started coding, but not so much that you are locked in to any certain mindset or methodology.

## Quick Summary! (TL;DR)

* Clone this repo (make sure your source is on GitLab)
* Change the occurences of `athena` to a name you like (name of file `athena.py`, variable in `.gitlab-ci.yml` & on last line in `Dockerfile`) 
* Setup Google Cloud correctly
* Add your service account JSON key to GitLab secrets section.
* Change the variables in `.gitlab-ci.yml`
* Remove the existing pyproject.toml file, run `poetry init` and `poetry add flask gunicorn python-json-logger` on the root directory.
* `poetry run python athena.py` will run the project locally 🤘🤘🤘
* Open http://localhost:8080/ in browser, see your awesome website
* Now make a commit and go be amazed at your wonderful GitLab pipline deploying your awesome stuff all by it self!
* Finally, write awesome code 🤓🤓🤓

**Read the full document for all the juicy details!**

## Shameless self-plug!

This boilerplate was created after I've started countless projects, each time refining it but still very similar.

My latest project [CityGuessr.fun](https://cityguessr.fun) use this boilerplate: A daily challenge webgame where you guess the city based on custom maps!

## Components

* `athena.py`: the main entrypoint for your webapp. Rename it to something appropriately. Expanding it, change it, (ab-)use it as you like!
  * When you change the file name of this file, you also need to change the reference to it on the last line of `Dockerfile`!
* `/static` & `/templates`: Special folders used by Flask. Static is for static files like images, javascript, css, etc. Templates is for flask template files.
* `.gitlab-ci.yml`: GitLab CI/CD file which defines your pipeline. If you are not familiar you can read https://docs.gitlab.com/ee/ci/
  * **ATTENTION:** You need to change the variables at the top of this file!
* `Dockerfile`: Docker build file. If you are not familiar maybe just google it?
* `pyproject.toml`: The main kinda "definition" file for Poetry. Not recommended to manually edit, let Poetry deal with it.
* `.gitignore`: Files ignored by git, I've added some sensible defaults.

## Prerequisites
* [Python](https://www.python.org/downloads/) (with [Poetry](https://python-poetry.org/docs/#installation)) installed and working
* [Docker](https://docs.docker.com/get-docker/) installed and working
* A [GitLab](https://gitlab.com/) account
* A [Google Cloud Platform](https://cloud.google.com/) account 

## Configuration & Setup

There's some manual steps which is difficult to automate in this boilerplate. Most of the GCP-stuff could be done in Terraform but I really don't want to start adding that -- Terraform is a beast of it's own!

**SO - LET'S GO!**

### GitLab
Create an empty project (and group if you want) in GitLab, then clone/copy this boilerplate repo and init your repo with this.

**ATTENTION:** You need to change the variables at the top of your `.gitlab-ci.yml` file!

### Source code
For the sake of keeping things simple I have used "Athena" as a name for this fictive project, so anywhere you find "athena" replace it with your own project name. Especially important for the file (name and content of) `athena.py` of course!

### Google Cloud Platform 
* Go to https://console.cloud.google.com/
* Enable these APIs: `Artifact Registry` (or `Container Registry`) & `Cloud Run` & whatever else you need
* Create a service account, create new key and export to JSON.
* Save the JSON key as `whatever_key.json` in your project directory. 
  * **NEEDS TO BE** `*_key.json` as this is in `.gitignore`!
* Also store JSON key in `GitLab->Settings->CI/CD->Variables` as `GOOGLE_CLOUD_ACCOUNT` type "variable".
  * Recommended: Create two keys; one for your local development and one for your GitLab pipelines.
* Go to IAM in GCP, find your service account, assign these roles: `Cloud Build Service Account, Cloud Run Admin, Storage Object Admin, Service Account User`
  * There are more optimal roles/permissions to assign but lets not worry about it now... GCP will also give you advise on this later.
* Take a note of your service account "principal" and put that in your `.gitlab-ci.yml` in the variable `GCP_ACCOUNT`

### Cloud Run
There's a couple of things you need to do manually **AFTER** you run your first successful deploy:
* Go to your service, click "Edit and deploy new revision"
* Tweak whatever you want here, pay attention to Capacity section. 
  * Strongly recommended: Set Auto-scaling minimum instances to 1. This is a tiny bit more expensive but you avoid really slow "cold starts". 
* TODO: Add more useful stuff here!

### Poetry
You need to initialize poetry in your local directory. *Alternatively* you can edit and use the included `pyproject.toml` but it's cleaner to do your own "fresh" poetry setup. 
* Go to your local directory for your new repository
* `poetry init`
* `poetry add flask gunicorn python-json-logger`

## GOOD TO GO! 🤯🤯🤯
* `poetry shell -> python athena.py` or `poetry run python athena.py`
* Open http://localhost:8080/ in browser, see your awesome website
* Each time you commit, your website/app will be automagically deployed to GCP Cloud Run available for the world to admire!

## Bonus objectives
* Custom domain
  * Buy a domain
  * Use GCP Cloud DNS as your registar
  * Setup a domain mapping for Cloud Run
* Probably you want to use more GCP services like Redis, SQL, GCS, etc etc
  * I highly recommend [Mongo Atlas](https://www.mongodb.com/atlas) for your Mongo-needs!
* TODO: add more clever stuff here!